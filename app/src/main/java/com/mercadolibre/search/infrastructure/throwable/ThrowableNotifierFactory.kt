package com.mercadolibre.search.infrastructure.throwable

object ThrowableNotifierFactory {
    fun create(): ThrowableNotifier {
        return ThrowableNotifier()
    }
}