package com.mercadolibre.search.infrastructure.repository

import com.mercadolibre.search.infrastructure.repository.representation.ProductDetailResponse
import com.mercadolibre.search.infrastructure.repository.representation.ProductSearchResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MercadoLibreClient {
    @GET("sites/MLA/search")
    fun findProducts(@Query("q") phrase: String): Single<ProductSearchResponse>

    @GET("items/{itemId}")
    fun getProductDetail(@Path("itemId") productId: String): Single<ProductDetailResponse>
}