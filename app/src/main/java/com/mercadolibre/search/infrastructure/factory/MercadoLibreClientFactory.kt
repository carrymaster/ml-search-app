package com.mercadolibre.search.infrastructure.factory

import com.mercadolibre.search.infrastructure.repository.MercadoLibreClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object MercadoLibreClientFactory {
    fun create(): MercadoLibreClient {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://api.mercadolibre.com/")
            .build()
            .create(MercadoLibreClient::class.java)
    }
}