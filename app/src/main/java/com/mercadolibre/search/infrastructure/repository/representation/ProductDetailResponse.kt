package com.mercadolibre.search.infrastructure.repository.representation

import com.google.gson.annotations.SerializedName

data class ProductDetailResponse(
    @SerializedName("id") val id: String,
    @SerializedName("title") val title: String,
    @SerializedName("price") val price: Float,
    @SerializedName("available_quantity") val availableQuantity: Int,
    @SerializedName("sold_quantity") val soldQuantity: Int,
    @SerializedName("permalink") val link: String,
    @SerializedName("pictures") val pictures: List<PictureResponse>
)

data class PictureResponse(@SerializedName("secure_url") val url: String)