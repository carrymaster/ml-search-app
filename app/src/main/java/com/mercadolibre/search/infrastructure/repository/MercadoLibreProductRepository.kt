package com.mercadolibre.search.infrastructure.repository

import com.mercadolibre.search.core.domain.Product
import com.mercadolibre.search.core.domain.ProductRepository
import com.mercadolibre.search.infrastructure.repository.representation.ProductSearchResponse
import io.reactivex.Single

class MercadoLibreProductRepository(private val mercadoLibreClient: MercadoLibreClient) : ProductRepository {

    override fun findByPhrase(phrase: String): Single<List<Product>> {
        return mercadoLibreClient.findProducts(phrase)
            .map { createProducts(it) }
    }

    private fun createProducts(productsSearchResponse: ProductSearchResponse) =
        productsSearchResponse.products.map { Product(it.id, it.title, it.price, it.thumbnail) }
}

