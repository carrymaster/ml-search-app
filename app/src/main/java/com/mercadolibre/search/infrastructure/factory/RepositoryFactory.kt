package com.mercadolibre.search.infrastructure.factory

import com.mercadolibre.search.core.domain.ProductDetailRepository
import com.mercadolibre.search.core.domain.ProductRepository
import com.mercadolibre.search.infrastructure.repository.MercadoLibreProductDetailRepository
import com.mercadolibre.search.infrastructure.repository.MercadoLibreProductRepository

object RepositoryFactory {
    fun createMercadoLibreProductsRepository(): ProductRepository {
        return MercadoLibreProductRepository(MercadoLibreClientFactory.create())
    }

    fun createMercadoLibreProductDetailRepository(): ProductDetailRepository {
        return MercadoLibreProductDetailRepository(MercadoLibreClientFactory.create())
    }
}