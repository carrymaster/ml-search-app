package com.mercadolibre.search.infrastructure.repository.representation

import com.google.gson.annotations.SerializedName

data class ProductSearchResponse(@SerializedName("results") val products: List<ProductResponse>)