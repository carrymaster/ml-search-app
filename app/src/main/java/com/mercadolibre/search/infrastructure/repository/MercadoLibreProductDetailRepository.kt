package com.mercadolibre.search.infrastructure.repository

import com.mercadolibre.search.core.domain.ProductDetail
import com.mercadolibre.search.core.domain.ProductDetailRepository
import com.mercadolibre.search.infrastructure.repository.representation.ProductDetailResponse
import io.reactivex.Single

class MercadoLibreProductDetailRepository(private val mercadoLibreClient: MercadoLibreClient) :
    ProductDetailRepository {

    override fun find(productId: String): Single<ProductDetail> {
        return mercadoLibreClient.getProductDetail(productId)
            .map { createProductDetail(it) }
    }

    private fun createProductDetail(productDetailResponse: ProductDetailResponse) =
        ProductDetail(
            productDetailResponse.id,
            productDetailResponse.title,
            productDetailResponse.price,
            productDetailResponse.availableQuantity,
            productDetailResponse.soldQuantity,
            productDetailResponse.link,
            productDetailResponse.pictures.map { it.url })
}