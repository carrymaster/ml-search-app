package com.mercadolibre.search.infrastructure.throwable

import com.mercadolibre.search.extensions.asSubject
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class ThrowableNotifier {
    fun notify(throwable: Throwable) {
        throwableObservable.asSubject()
            .onNext(throwable)
    }

    companion object {
        val throwableObservable: Observable<Throwable> = PublishSubject.create()
    }
}