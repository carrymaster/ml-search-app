package com.mercadolibre.search.extensions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

fun <T> LiveData<T>.setValue(value: T) {
    (this as MutableLiveData).value = value
}