package com.mercadolibre.search.extensions

import io.reactivex.Observable
import io.reactivex.subjects.Subject

fun <T> Observable<T>.asSubject(): Subject<T> {
    return this as Subject<T>
}