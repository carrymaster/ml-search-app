package com.mercadolibre.search.extensions

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

fun <T> LifecycleOwner.onChange(liveData: LiveData<T>, action: (T) -> Unit) {
    liveData.observe(this, Observer { value ->
        value?.also { action(it) }
    })
}