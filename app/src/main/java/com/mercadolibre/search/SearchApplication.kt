package com.mercadolibre.search

import android.app.Application
import android.util.Log
import com.mercadolibre.search.infrastructure.throwable.ThrowableNotifier
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class SearchApplication : Application() {

    private val compositeDisposable = CompositeDisposable()

    override fun onCreate() {
        super.onCreate()
        configureRxDefaultErrorHandler()
        configureThrowableLogger()
    }

    override fun onTerminate() {
        super.onTerminate()
        compositeDisposable.clear()
    }

    private fun configureThrowableLogger() {
        ThrowableNotifier.throwableObservable
            .subscribeBy {
                Log.e(it::class.java.simpleName, it.message, it)
            }
            .addTo(compositeDisposable)
    }

    private fun configureRxDefaultErrorHandler() {
        RxJavaPlugins.setErrorHandler {
            Log.d("RxDefaultErrorHandler", it.message, it)
        }
    }

}