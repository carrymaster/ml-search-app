package com.mercadolibre.search.core.domain

import io.reactivex.Single

interface ProductDetailRepository {
    fun find(productId: String): Single<ProductDetail>
}