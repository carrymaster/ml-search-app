package com.mercadolibre.search.core.action

import com.mercadolibre.search.core.domain.ProductDetail
import com.mercadolibre.search.core.domain.ProductDetailRepository
import io.reactivex.Single

class FindProductDetail(private val productDetailRepository: ProductDetailRepository) {
    operator fun invoke(productId: String): Single<ProductDetail> {
        return productDetailRepository.find(productId)
    }
}