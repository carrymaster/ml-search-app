package com.mercadolibre.search.core.factory

import com.mercadolibre.search.core.action.FindProductDetail
import com.mercadolibre.search.core.action.SearchProducts
import com.mercadolibre.search.infrastructure.factory.RepositoryFactory

object ActionFactory {
    fun createSearchProducts(): SearchProducts {
        return SearchProducts(RepositoryFactory.createMercadoLibreProductsRepository())
    }

    fun createFindProductDetail(): FindProductDetail {
        return FindProductDetail(RepositoryFactory.createMercadoLibreProductDetailRepository())
    }
}