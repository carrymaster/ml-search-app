package com.mercadolibre.search.core.action

import com.mercadolibre.search.core.domain.Product
import com.mercadolibre.search.core.domain.ProductRepository
import io.reactivex.Single

class SearchProducts(private val productRepository: ProductRepository) {
    operator fun invoke(phrase: String): Single<List<Product>> {
        return productRepository.findByPhrase(phrase)
    }
}