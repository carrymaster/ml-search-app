package com.mercadolibre.search.core.domain

data class Product(val id: String,
                   val title: String,
                   val price: Float,
                   val thumbnail: String)