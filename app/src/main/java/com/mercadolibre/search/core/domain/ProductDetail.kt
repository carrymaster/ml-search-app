package com.mercadolibre.search.core.domain

data class ProductDetail(val id: String,
                         val title: String,
                         val price: Float,
                         val availableQuantity: Int,
                         val soldQuantity: Int,
                         val link: String,
                         val pictures: List<String>)