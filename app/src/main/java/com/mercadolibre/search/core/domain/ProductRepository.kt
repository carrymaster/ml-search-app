package com.mercadolibre.search.core.domain

import io.reactivex.Single

interface ProductRepository {
    fun findByPhrase(phrase: String): Single<List<Product>>
}