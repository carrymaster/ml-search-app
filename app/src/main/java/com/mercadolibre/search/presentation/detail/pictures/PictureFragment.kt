package com.mercadolibre.search.presentation.detail.pictures

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.mercadolibre.search.R
import kotlinx.android.synthetic.main.view_product_picture_item.*

class PictureFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_product_picture_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showImage()
    }

    private fun showImage() {
        Glide.with(context!!)
            .load(arguments?.getString("URL")!!)
            .into(pictureImageView)
    }

    companion object {
        fun newFragment(pictureUrl: String): PictureFragment {
            return PictureFragment()
                .also { it.arguments = Bundle().also { it.putString("URL", pictureUrl) } }
        }
    }
}