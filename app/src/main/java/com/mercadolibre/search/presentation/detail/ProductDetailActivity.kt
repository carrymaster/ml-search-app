package com.mercadolibre.search.presentation.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import com.mercadolibre.search.R
import com.mercadolibre.search.extensions.onChange
import com.mercadolibre.search.presentation.detail.viewmodel.ProductDetailStatus
import com.mercadolibre.search.presentation.detail.viewmodel.ProductDetailViewModelFactory
import kotlinx.android.synthetic.main.activity_product_detail.*

class ProductDetailActivity : AppCompatActivity() {

    private val productId
        get() = intent?.extras?.getString(PRODUCT_ID_EXTRA)
            ?: throw IllegalArgumentException("Product Id cannot be null")

    private val viewModel by lazy { ProductDetailViewModelFactory.create(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)
        configureActionBar()
        viewModel.init(productId)
        observeProductDetailStatus()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun configureActionBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Detalle"
    }

    private fun observeProductDetailStatus() {
        onChange(viewModel.productDetailStatusLiveData) {
            when (it) {
                ProductDetailStatus.SEARCHING -> loadingProgressBar.visibility = View.VISIBLE
                ProductDetailStatus.FINISHED -> { loadingProgressBar.visibility = View.GONE }
            }
        }
    }

    companion object {
        private const val PRODUCT_ID_EXTRA = "PRODUCT_ID_EXTRA"

        fun createIntent(context: Context, productId: String): Intent {
            return Intent(context, ProductDetailActivity::class.java).also {
                it.putExtra(PRODUCT_ID_EXTRA, productId)
            }
        }
    }
}