package com.mercadolibre.search.presentation.search.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mercadolibre.search.R

class ProductsRecyclerViewAdapter : RecyclerView.Adapter<ProductItem.ViewHolder>() {

    private var productItems = listOf<ProductItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductItem.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_product_item, parent, false)
        return ProductItem.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductItem.ViewHolder, position: Int) {
        productItems[position].bind(holder)
    }

    override fun getItemCount(): Int {
        return productItems.size
    }

    fun replaceAll(productItems: List<ProductItem>) {
        this.productItems = productItems
    }
}