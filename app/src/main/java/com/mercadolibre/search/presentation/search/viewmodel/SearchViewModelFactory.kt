package com.mercadolibre.search.presentation.search.viewmodel

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.mercadolibre.search.core.factory.ActionFactory

object SearchViewModelFactory {
    fun create(activity: FragmentActivity): SearchViewModel {
        return ViewModelProviders.of(activity, createFactory())
            .get(SearchViewModel::class.java)
    }

    private fun createFactory(): ViewModelProvider.Factory {
        return object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return SearchViewModel(
                    ActionFactory.createSearchProducts()
                ) as T
            }
        }
    }
}