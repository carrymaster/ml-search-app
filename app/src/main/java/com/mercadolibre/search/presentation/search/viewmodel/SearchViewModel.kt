package com.mercadolibre.search.presentation.search.viewmodel


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hadilq.liveevent.LiveEvent
import com.mercadolibre.search.core.action.SearchProducts
import com.mercadolibre.search.core.domain.Product
import com.mercadolibre.search.extensions.setValue
import com.mercadolibre.search.presentation.search.viewmodel.SearchStatus.FINISHED
import com.mercadolibre.search.presentation.search.viewmodel.SearchStatus.SEARCHING
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class SearchViewModel(
    private val searchProducts: SearchProducts
) : ViewModel() {

    val productsLiveData: LiveData<List<Product>> = MutableLiveData()
    val searchStatusLiveData: LiveData<SearchStatus> = MutableLiveData()
    val errorLiveData = LiveEvent<Throwable>()
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun search(phrase: String) {
        resetValues()
        startSearch(phrase)
    }

    private fun startSearch(phrase: String) {
        searchProducts(phrase)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .doOnSubscribe { searchStatusLiveData.setValue(SEARCHING) }
            .doFinally { searchStatusLiveData.setValue(FINISHED) }
            .subscribeBy(
                onSuccess = {
                    productsLiveData.setValue(it)
                },
                onError = {
                    errorLiveData.value = it
                }
            )
            .addTo(compositeDisposable)
    }

    private fun resetValues() {
        compositeDisposable.clear()
        productsLiveData.setValue(emptyList())
    }

    override fun onCleared() {
        compositeDisposable.clear()
    }
}

enum class SearchStatus {
    SEARCHING,
    FINISHED
}
