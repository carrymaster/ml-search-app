package com.mercadolibre.search.presentation.search

import android.os.Bundle
import android.util.Log
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import com.mercadolibre.search.R
import com.mercadolibre.search.presentation.search.viewmodel.SearchViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*


class SearchActivity : AppCompatActivity() {

    private val searchViewModel by lazy { SearchViewModelFactory.create(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Buscar productos"
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main_activity, menu)
        val search = menu?.findItem(R.id.action_search) ?: return false
        val searchView = search.actionView as SearchView
        searchView.queryHint = "Search"

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                searchViewModel.search(query)
                searchView.clearFocus()
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                Log.d("test", "Searching new word: $newText")
                return false
            }
        })

        return super.onCreateOptionsMenu(menu)
    }
}
