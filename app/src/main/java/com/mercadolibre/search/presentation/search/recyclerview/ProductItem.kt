package com.mercadolibre.search.presentation.search.recyclerview

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mercadolibre.search.R
import com.mercadolibre.search.core.domain.Product
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat

class ProductItem(private val product: Product, private val onClickAction: () -> Unit) {

    private val numberFormat get() = NumberFormatFactory.createDefault()

    fun bind(holder: ViewHolder) {
        holder.itemView.setOnClickListener { onClickAction() }
        holder.titleTextView.text = product.title
        holder.priceTextView.text = "$${numberFormat.format(product.price)}"
        Glide.with(holder.itemView)
            .load(product.thumbnail)
            .into(holder.thumbnailImageView)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val titleTextView = view.findViewById<TextView>(R.id.title)!!
        val priceTextView = view.findViewById<TextView>(R.id.price)!!
        val thumbnailImageView = view.findViewById<ImageView>(R.id.thumbnail)!!
    }
}

object NumberFormatFactory {
    fun createDefault(): NumberFormat {
        val numberFormat = NumberFormat.getInstance() as DecimalFormat
        val decimalFormatSymbols = DecimalFormatSymbols()
            .also { it.decimalSeparator = ',' }
            .also { it.groupingSeparator = '.' }
        numberFormat.decimalFormatSymbols = decimalFormatSymbols
        return numberFormat
    }
}