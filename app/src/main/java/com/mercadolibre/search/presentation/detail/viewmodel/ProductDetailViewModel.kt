package com.mercadolibre.search.presentation.detail.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hadilq.liveevent.LiveEvent
import com.mercadolibre.search.core.action.FindProductDetail
import com.mercadolibre.search.core.domain.ProductDetail
import com.mercadolibre.search.extensions.setValue
import com.mercadolibre.search.presentation.detail.viewmodel.ProductDetailStatus.FINISHED
import com.mercadolibre.search.presentation.detail.viewmodel.ProductDetailStatus.SEARCHING
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class ProductDetailViewModel(private val findProductDetail: FindProductDetail) : ViewModel() {

    val productDetailLiveData: LiveData<ProductDetail> = MutableLiveData()
    val productDetailStatusLiveData: LiveData<ProductDetailStatus> = MutableLiveData()
    val errorLiveData: LiveEvent<Throwable> = LiveEvent()

    private val compositeDisposable = CompositeDisposable()

    fun init(productId: String) {
        if (isAlreadyExecuted()) return
        startFindProductDetail(productId)
    }

    private fun isAlreadyExecuted() = productDetailStatusLiveData.value != null

    private fun startFindProductDetail(productId: String) {
        findProductDetail(productId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .doOnSubscribe { productDetailStatusLiveData.setValue(SEARCHING) }
            .doFinally { productDetailStatusLiveData.setValue(FINISHED) }
            .subscribeBy(
                onSuccess = {
                    productDetailLiveData.setValue(it)
                },
                onError = {
                    errorLiveData.value = it
                }
            )
            .addTo(compositeDisposable)
    }

    override fun onCleared() {
        compositeDisposable.clear()
    }
}

enum class ProductDetailStatus {
    SEARCHING, FINISHED
}