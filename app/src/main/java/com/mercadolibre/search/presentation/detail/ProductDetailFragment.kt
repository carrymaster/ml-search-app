package com.mercadolibre.search.presentation.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.mercadolibre.search.R
import com.mercadolibre.search.core.domain.ProductDetail
import com.mercadolibre.search.extensions.onChange
import com.mercadolibre.search.infrastructure.throwable.ThrowableNotifierFactory
import com.mercadolibre.search.presentation.detail.pictures.PicturePagerAdapter
import com.mercadolibre.search.presentation.detail.viewmodel.ProductDetailViewModelFactory
import com.mercadolibre.search.presentation.search.recyclerview.NumberFormatFactory
import kotlinx.android.synthetic.main.fragment_product_detail.*

class ProductDetailFragment : Fragment() {

    private val throwableNotifier by lazy { ThrowableNotifierFactory.create() }
    private var errorAlert: AlertDialog? = null
    private val numberFormat = NumberFormatFactory.createDefault()
    private val viewModel by lazy { ProductDetailViewModelFactory.create(activity!!) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        restoreAlertDialog(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_product_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeProductDetail()
        observeError()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(ERROR_DIALOG_VISIBLE, errorAlert?.isShowing ?: false)
    }

    override fun onDestroy() {
        super.onDestroy()
        errorAlert?.dismiss()
    }

    private fun observeError() {
        onChange(viewModel.errorLiveData) {
            throwableNotifier.notify(it)
            showErrorAlert()
        }
    }

    private fun observeProductDetail() {
        onChange(viewModel.productDetailLiveData) { productDetail ->
            soldQuantityTextView.text = "${productDetail.soldQuantity} vendidos"
            availableQuantityTextView.text = "Quedan ${productDetail.availableQuantity} disponibles!"
            titleTextView.text = productDetail.title
            priceTextView.text = "$${numberFormat.format(productDetail.price)}"
            viewMoreButton.setOnClickListener { openProductLink(productDetail) }
            bindProductPictures(productDetail)
            detailContentView.visibility = View.VISIBLE
        }
    }

    private fun openProductLink(productDetail: ProductDetail) {
        val intent = Intent(Intent.ACTION_VIEW).also { it.data =  Uri.parse(productDetail.link)}
        startActivity(intent)
    }

    private fun bindProductPictures(productDetail: ProductDetail) {
        picturesViewPager.adapter = PicturePagerAdapter(
            childFragmentManager,
            productDetail
        )
        viewPagerIndicator.setViewPager(picturesViewPager)
        picturesViewPager.adapter?.registerDataSetObserver(viewPagerIndicator.dataSetObserver)
    }

    private fun showErrorAlert() {
        errorAlert = createErrorAlertDialog()
        errorAlert?.show()
    }

    private fun createErrorAlertDialog(): AlertDialog {
        return AlertDialog.Builder(requireContext())
            .setTitle("Error")
            .setMessage("Ocurrio un error. Intente mas tarde.")
            .setPositiveButton("OK") { _, _ -> activity?.finish() }
            .setCancelable(false)
            .create()
    }

    private fun restoreAlertDialog(savedInstanceState: Bundle?) {
        savedInstanceState?.getBoolean(ERROR_DIALOG_VISIBLE)
            ?.also { mustShowError ->
                if (mustShowError) showErrorAlert()
            }
    }

    companion object {
        private const val ERROR_DIALOG_VISIBLE = "ERROR_DIALOG_VISIBLE"
    }
}