package com.mercadolibre.search.presentation.detail.pictures

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.mercadolibre.search.core.domain.ProductDetail

class PicturePagerAdapter(fragmentManager: FragmentManager, private val productDetail: ProductDetail) : FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return PictureFragment.newFragment(productDetail.pictures[position])
    }

    override fun getCount(): Int {
        return productDetail.pictures.size
    }
}