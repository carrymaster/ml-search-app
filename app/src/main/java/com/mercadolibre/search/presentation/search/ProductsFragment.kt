package com.mercadolibre.search.presentation.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.mercadolibre.search.R
import com.mercadolibre.search.core.domain.Product
import com.mercadolibre.search.extensions.onChange
import com.mercadolibre.search.infrastructure.throwable.ThrowableNotifierFactory
import com.mercadolibre.search.presentation.detail.ProductDetailActivity
import com.mercadolibre.search.presentation.search.viewmodel.SearchStatus
import com.mercadolibre.search.presentation.search.viewmodel.SearchViewModelFactory
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_products.*

class ProductsFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()
    private var errorDialog: AlertDialog? = null
    private val throwableNotifier by lazy { ThrowableNotifierFactory.create() }
    private val searchViewModel by lazy { SearchViewModelFactory.create(activity!!) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        restoreAlertDialog(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_products, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeSearchStatus()
        observeProducts()
        observeProductSelection()
        observeSearchErrors()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(ERROR_DIALOG_VISIBLE, errorDialog?.isShowing ?: false)
    }

    override fun onDestroy() {
        super.onDestroy()
        errorDialog?.dismiss()
        compositeDisposable.clear()
    }

    private fun observeSearchStatus() {
        onChange(searchViewModel.searchStatusLiveData) {
            when (it) {
                SearchStatus.SEARCHING -> {
                    emptyListGroup.visibility = View.GONE
                    loadingProgressBar.visibility = View.VISIBLE
                }
                SearchStatus.FINISHED -> loadingProgressBar.visibility = View.GONE
            }
        }
    }

    private fun observeProducts() {
        onChange(searchViewModel.productsLiveData) {
            if (it.isEmpty()) {
                emptyListTextView.text = "Parece que no hay resultados para esa busqueda.\nIntenta con otra palabra."
                emptyListGroup.visibility = View.VISIBLE
            } else {
                emptyListGroup.visibility = View.GONE
            }
            showProducts(it)
        }
    }

    private fun observeProductSelection() {
        productsRecyclerView.productSelectedObservable
            .subscribeBy { goToProductDetail(it) }
            .addTo(compositeDisposable)
    }

    private fun observeSearchErrors() {
        onChange(searchViewModel.errorLiveData) {
            showErrorAlert()
            throwableNotifier.notify(it)
        }
    }

    private fun showErrorAlert() {
        errorDialog = createErrorAlertDialog()
        errorDialog?.show()
    }

    private fun createErrorAlertDialog(): AlertDialog {
        return AlertDialog.Builder(requireContext())
            .setTitle("Error")
            .setMessage("Ocurrio un error. Intente mas tarde.")
            .setPositiveButton("OK") { _, _ -> }
            .setCancelable(false)
            .create()
    }

    private fun showProducts(products: List<Product>) {
        productsRecyclerView.showProducts(products)
    }

    private fun goToProductDetail(product: Product) {
        startActivity(ProductDetailActivity.createIntent(requireContext(), product.id))
    }

    private fun restoreAlertDialog(savedInstanceState: Bundle?) {
        savedInstanceState?.getBoolean(ERROR_DIALOG_VISIBLE)
            ?.also { mustShowError ->
                if (mustShowError) showErrorAlert()
            }
    }

    companion object {
        private const val ERROR_DIALOG_VISIBLE = "ERROR_DIALOG_VISIBLE"
    }
}