package com.mercadolibre.search.presentation.detail.viewmodel

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.mercadolibre.search.core.factory.ActionFactory

object ProductDetailViewModelFactory {
    fun create(activity: FragmentActivity): ProductDetailViewModel {
        return ViewModelProviders.of(activity,
            createFactory()
        )
            .get(ProductDetailViewModel::class.java)
    }

    private fun createFactory(): ViewModelProvider.Factory {
        return object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return ProductDetailViewModel(ActionFactory.createFindProductDetail()) as T
            }
        }
    }
}