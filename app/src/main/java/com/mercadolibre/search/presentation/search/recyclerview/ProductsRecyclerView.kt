package com.mercadolibre.search.presentation.search.recyclerview

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mercadolibre.search.core.domain.Product
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject


class ProductsRecyclerView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) :
    RecyclerView(context, attrs, defStyle) {

    private val productSelectedSubject: PublishSubject<Product> = PublishSubject.create()
    val productSelectedObservable = productSelectedSubject as Observable<Product>

    private val productsAdapter = ProductsRecyclerViewAdapter()

    init {
        layoutManager = LinearLayoutManager(context, VERTICAL, false)
        adapter = productsAdapter
        addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
    }

    fun showProducts(products: List<Product>) {
        products.map { createProductItem(it) }
            .also { productsAdapter.replaceAll(it) }
        productsAdapter.notifyDataSetChanged()
    }

    private fun createProductItem(product: Product) =
        ProductItem(product) { productSelectedSubject.onNext(product) }
}