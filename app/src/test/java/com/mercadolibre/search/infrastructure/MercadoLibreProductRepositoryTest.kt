package com.mercadolibre.search.infrastructure

import com.mercadolibre.search.core.domain.Product
import com.mercadolibre.search.infrastructure.repository.MercadoLibreClient
import com.mercadolibre.search.infrastructure.repository.MercadoLibreProductRepository
import com.mercadolibre.search.infrastructure.repository.representation.ProductResponse
import com.mercadolibre.search.infrastructure.repository.representation.ProductSearchResponse
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Test

class MercadoLibreProductRepositoryTest {

    private lateinit var mercadoLibreClient: MercadoLibreClient
    private lateinit var testObserver: TestObserver<List<Product>>
    private val mercadoLibreProductRepository by lazy {
        MercadoLibreProductRepository(
            mercadoLibreClient
        )
    }

    @Test
    fun `find by phrase should return correct products`() {
        givenAMercadoLibreClient()

        whenFinding()

        thenCorrectProductsAreReturned()
    }

    private fun givenAMercadoLibreClient() {
        mercadoLibreClient = mock {
            on { findProducts(searchedPhrase) } doReturn Single.just(
                ProductSearchResponse(
                    productSearchResponse
                )
            )
        }
    }

    private fun whenFinding() {
        testObserver = mercadoLibreProductRepository.findByPhrase(searchedPhrase).test()
    }

    private fun thenCorrectProductsAreReturned() {
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValue { it == expectedProducts }
    }

    companion object {
        private const val searchedPhrase = "iPhone"

        private val productSearchResponse = listOf(
            ProductResponse("101", "iPhone", "www.google.com.ar", 1500.5f),
            ProductResponse("102", "iPhone X", "www.google.com.ar/thumnail", 2500.5f)
        )

        private val expectedProducts = listOf(
            Product("101", "iPhone", 1500.5f, "www.google.com.ar"),
            Product("102", "iPhone X", 2500.5f,"www.google.com.ar/thumnail")
        )
    }
}