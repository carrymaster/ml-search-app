package com.mercadolibre.search.infrastructure.repository

import com.mercadolibre.search.core.domain.ProductDetail
import com.mercadolibre.search.infrastructure.repository.representation.PictureResponse
import com.mercadolibre.search.infrastructure.repository.representation.ProductDetailResponse
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Test

class MercadoLibreProductDetailRepositoryTest {

    private lateinit var mercadoLibreClient: MercadoLibreClient
    private val mercadoLibreProductDetailRepository by lazy {
        MercadoLibreProductDetailRepository(mercadoLibreClient)
    }
    private lateinit var testObserver: TestObserver<ProductDetail>

    @Test
    fun `find should return product detail`() {
        givenAMercadoLibreClient()

        whenFinding()

        thenProductDetailIsReturned()
    }

    private fun givenAMercadoLibreClient() {
        mercadoLibreClient = mock {
            on { it.getProductDetail(PRODUCT_ID) } doReturn Single.just(productDetailResponse)
        }
    }

    private fun whenFinding() {
        testObserver = mercadoLibreProductDetailRepository.find(PRODUCT_ID).test()
    }

    private fun thenProductDetailIsReturned() {
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValue { it == expectedProductDetail }
    }

    companion object {
        private const val PRODUCT_ID = "ML101"
        private val productDetailResponse = ProductDetailResponse(
            PRODUCT_ID,
            "iPhone X",
            1000.5f,
            100,
            20,
            "www.google.com.ar",
            listOf(PictureResponse("www.picture.com.ar"), PictureResponse("www.otherpicture.com.ar"))
        )
        private val expectedProductDetail = ProductDetail(
            PRODUCT_ID,
            "iPhone X",
            1000.5f,
            100,
            20,
            "www.google.com.ar",
            listOf("www.picture.com.ar", "www.otherpicture.com.ar")
        )
    }
}