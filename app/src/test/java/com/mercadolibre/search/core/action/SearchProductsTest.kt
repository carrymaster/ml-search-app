package com.mercadolibre.search.core.action

import com.mercadolibre.search.core.domain.Product
import com.mercadolibre.search.core.domain.ProductRepository
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Test

class SearchProductsTest {

    private lateinit var productRepository: ProductRepository
    private lateinit var searchedPhrase: String
    private lateinit var testObserver: TestObserver<List<Product>>
    private val expectedProducts = listOf(
        Product("101", "iPhone", 100f, "www.google.com.ar"),
        Product("102", "iPhone X", 1000.5f, "www.thumbnail-me.com")
    )
    private val searchProducts by lazy { SearchProducts(productRepository) }

    @Test
    fun `search products`() {
        givenASearchedPhrase("iPhone")
        givenAProductRepository()

        whenSearching()

        thenProductsAreReturned()
    }

    private fun givenASearchedPhrase(phrase: String) {
        searchedPhrase = phrase
    }

    private fun givenAProductRepository() {
        productRepository = mock {
            on { findByPhrase(searchedPhrase) } doReturn Single.just(expectedProducts)
        }
    }

    private fun whenSearching() {
        testObserver = searchProducts(searchedPhrase).test()
    }

    private fun thenProductsAreReturned() {
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValue { it == expectedProducts }
    }

}