package com.mercadolibre.search.core.action

import com.mercadolibre.search.core.domain.ProductDetail
import com.mercadolibre.search.core.domain.ProductDetailRepository
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Test

class FindProductDetailTest {

    private lateinit var productDetailRepository: ProductDetailRepository
    private val findProductDetail by lazy { FindProductDetail(productDetailRepository) }
    private lateinit var testObserver: TestObserver<ProductDetail>

    @Test
    fun `find product detail`() {
        givenAProductDetailRepository()

        whenFinding()

        thenProductDetailIsReturned()
    }

    private fun thenProductDetailIsReturned() {
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValue { it == expectedProductDetail }
    }

    private fun whenFinding() {
        testObserver = findProductDetail(PRODUCT_ID).test()
    }

    private fun givenAProductDetailRepository() {
        productDetailRepository = mock {
            on { find(PRODUCT_ID) } doReturn Single.just(expectedProductDetail)
        }
    }

    companion object {
        private const val PRODUCT_ID = "ML101"
        private val expectedProductDetail = ProductDetail(
            PRODUCT_ID,
            "iPhone X",
            1000.5f,
            100,
            20,
            "www.google.com.ar",
            listOf("www.picture.com.ar", "otherpicuture.com.ar")
        )
    }
}