package com.mercadolibre.search.presentation.search

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.mercadolibre.search.core.action.SearchProducts
import com.mercadolibre.search.core.domain.Product
import com.mercadolibre.search.presentation.search.viewmodel.SearchStatus.FINISHED
import com.mercadolibre.search.presentation.search.viewmodel.SearchStatus.SEARCHING
import com.mercadolibre.search.presentation.search.viewmodel.SearchViewModel
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Single
import org.assertj.core.api.BDDAssertions.then
import org.junit.Rule
import org.junit.Test


class SearchViewModelTest {

    @get:Rule
    val rxImmediateSchedulerRule = RxImmediateSchedulerRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var searchProducts: SearchProducts
    private val searchViewModel by lazy { SearchViewModel(searchProducts) }

    private val searchStatus get() = searchViewModel.searchStatusLiveData.value
    private val products get() = searchViewModel.productsLiveData.value
    private val throwable get() = searchViewModel.errorLiveData.value

    @Test
    fun `products should be posted when search finished`() {
        givenASearchProducts()

        whenSearching()

        then(products).isEqualTo(expectedProducts)
        then(searchStatus).isEqualTo(FINISHED)
    }

    @Test
    fun `products and status should be reset when a search start`() {
        givenANeverEndsSearchProducts()

        whenSearching()

        then(products).isEqualTo(emptyList<Product>())
        then(searchStatus).isEqualTo(SEARCHING)
    }

    @Test
    fun `status should be searching while search is in progress`() {
        givenANeverEndsSearchProducts()

        whenSearching()

        then(searchStatus).isEqualTo(SEARCHING)
    }

    @Test
    fun `error should be posted when search fails`() {
        givenASearchProductsFailed()

        whenSearching()

        then(throwable).isEqualTo(expectedThrowable)
        then(searchStatus).isEqualTo(FINISHED)
    }

    private fun givenASearchProductsFailed() {
        searchProducts = mock {
            on { invoke(searchedPhrase) } doReturn Single.error(expectedThrowable)
        }
    }

    private fun givenANeverEndsSearchProducts() {
        searchProducts = mock {
            on { invoke(searchedPhrase) } doReturn Single.never()
        }
    }

    private fun givenASearchProducts() {
        searchProducts = mock {
            on { invoke(searchedPhrase) } doReturn Single.just(expectedProducts)
        }
    }

    private fun whenSearching() {
        searchViewModel.search(searchedPhrase)
    }


    companion object {
        private const val searchedPhrase = "iPhone"
        private val expectedProducts = listOf(
            Product("101", "iPhone", 1500f, "www.google.com.ar"),
            Product("102", "iPhone X", 2500.54f, "www.google.com.ar")
        )
        private val expectedThrowable = RuntimeException()
    }
}