package com.mercadolibre.search.presentation.detail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.mercadolibre.search.core.action.FindProductDetail
import com.mercadolibre.search.core.domain.ProductDetail
import com.mercadolibre.search.presentation.detail.viewmodel.ProductDetailStatus
import com.mercadolibre.search.presentation.detail.viewmodel.ProductDetailViewModel
import com.mercadolibre.search.presentation.search.RxImmediateSchedulerRule
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import org.assertj.core.api.BDDAssertions.then
import org.junit.Rule
import org.junit.Test

class ProductDetailViewModelTest {

    @get:Rule
    val rxImmediateSchedulerRule = RxImmediateSchedulerRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var findProductDetail: FindProductDetail
    private val viewModel by lazy {
        ProductDetailViewModel(
            findProductDetail
        )
    }

    private val productDetail get() = viewModel.productDetailLiveData.value
    private val productDetailStatus get() = viewModel.productDetailStatusLiveData.value
    private val error get() = viewModel.errorLiveData.value

    @Test
    fun `init should post product detail`() {
        givenAFindProductDetail()

        whenInitiating()

        then(productDetail).isEqualTo(expectProductDetail)
        then(productDetailStatus).isEqualTo(ProductDetailStatus.FINISHED)
    }

    @Test
    fun `find product detail should be executed only once`() {
        givenAFindProductDetail()

        whenInitiating()
        whenInitiating()

        verify(findProductDetail, times(1)).invoke(PRODUCT_ID)
    }

    @Test
    fun `status should be loading while action is in progress`() {
        givenANeverEndsFindProductDetail()

        whenInitiating()

        then(productDetail).isNull()
        then(productDetailStatus).isEqualTo(ProductDetailStatus.SEARCHING)
    }

    @Test
    fun `error should be posted when find product details fails`() {
        findProductDetail = mock {
            on { invoke(PRODUCT_ID) } doReturn Single.error(expectedThrowable)
        }

        whenInitiating()

        then(productDetail).isNull()
        then(error).isEqualTo(expectedThrowable)
    }

    private fun givenANeverEndsFindProductDetail() {
        findProductDetail = mock {
            on { invoke(PRODUCT_ID) } doReturn Single.never()
        }
    }


    private fun givenAFindProductDetail() {
        findProductDetail = mock {
            on { invoke(PRODUCT_ID) } doReturn Single.just(expectProductDetail)
        }
    }

    private fun whenInitiating() {
        viewModel.init(PRODUCT_ID)
    }

    companion object {
        private const val PRODUCT_ID = "ML101"
        private val expectedThrowable = RuntimeException()
        private val expectProductDetail = ProductDetail(
            PRODUCT_ID,
            "iPhone X",
            1000.5f,
            100,
            20,
            "www.google.com.ar",
            listOf("www.picture.com.ar", "otherpicuture.com.ar")
        )
    }
}